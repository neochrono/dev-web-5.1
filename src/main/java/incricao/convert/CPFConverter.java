package incricao.convert;

import java.text.ParseException;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.swing.text.MaskFormatter;

@FacesConverter("cpfConverter")
public class CPFConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		return value;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		try {
			String cpf = (String) value;
			cpf = cpf.replaceAll("\\.|-", "");
			MaskFormatter formatter = new MaskFormatter("###.###.###-##");
			formatter.setValueContainsLiteralCharacters(false);
			return formatter.valueToString(cpf);
		} catch (ParseException e) {
			FacesMessage msg = new FacesMessage(String.format("CPF '%s' inválido", value.toString()));
            throw new ConverterException(msg);
		}
	}

}
